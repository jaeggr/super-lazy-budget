
# Super Lazy Budget

Envelope method budget tool in Python. This tool calculates how much needs to be in each Expense based on current day, or day given by argument, and remaining paychecks before the due date. Expenses are totaled into Envelopes, letting you know how much needs to be added to each to meet Expense goals.

## Required

- toml: `pip install toml`

## Configure

Configure each expense in an easy to manage config.toml file

```toml
[config]
envelopes = ["bills", "home"]
pay_iso_week = "odd"
payday = "Fri"
min_expense_saved_percent = 0.30

[[expenses]]
name = "Bills Visa"
due_day = 26
due_dates = []
amount = 1150.00
envelope = "bills"

[[expenses]]
name = "Internet"
due_day = 1
due_dates = []
amount = 81.00
envelope = "bills"

[[expenses]]
name = "Mortgage"
due_day = 31
due_dates = []
amount = 1120.00
envelope = "home"

[[expenses]]
name = "Water Util"
due_dates = ["2/18", "5/18", "8/18", "11/18"]
amount = 190.00
envelope = "bills"

[[expenses]]
name = "Snowball Payment"
due_day = 15
due_dates = []
amount = 150.00
envelope = "bills"
```

## Usage

`python .\budget.py`

or

`python .\budget.py --today 2024-04-26`

```
Expense              Envelope   Due        Percent    Amount
-----------------------------------------------------------------
Bills Visa           bills      04/26      1.0        1150.00
Internet             bills      05/01      1.0        81.00
Mortgage             home       04/30      1.0        1120.00
Water Util           bills      05/18      0.7        133.00
Snowball Payment     bills      05/15      0.7        105.00
-----------------------------------------------------------------
enter current amount in bills:  956
enter current amount in home:   800

Envelopes:
Envelope        Total Savings   Remaining
----------------------------------------
bills           1469.00         513.00
home            1120.00         320.00
----------------------------------------
```
