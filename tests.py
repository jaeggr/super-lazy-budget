
import unittest
from datetime import datetime, timedelta
from calendar import monthrange

from budget import Expense


class TestExpense(unittest.TestCase):
	def setUp(self):
		amount_value = ''
		current_date_value = datetime.now()
		self.expense = Expense("Test", 100, amount_value, current_date_value)

	def test_calculate_next_due_date(self):
		start_date = datetime(2024, 1, 1)
		end_date = datetime(2077, 1, 31)
		delta = timedelta(days=1)

		while start_date <= end_date:
			_, last_day_of_month = monthrange(start_date.year, start_date.month)
			for due_day in range(1, last_day_of_month + 1):  # Test all possible due days
				self.expense.due_day = due_day
				next_due_date = self.expense.calculate_next_due_date(start_date)

				if next_due_date:
					self.assertEqual(next_due_date.day, due_day)

				start_date += delta

	def test_calculate_next_due_date_with_due_dates(self):
		start_date = datetime(2024, 1, 1)
		end_date = datetime(2077, 1, 31)
		delta = timedelta(days=1)

		while start_date <= end_date:
			_, last_day_of_month = monthrange(start_date.year, start_date.month)
			_, last_day_of_next_month = monthrange(start_date.year, start_date.month % 12 + 1)
			for day in range(1, last_day_of_month + 1):  # Test all possible due days
				due_dates = [f"{start_date.year}/{start_date.month}/{day}"]
				if day <= last_day_of_next_month:
					due_dates.append(f"{start_date.year}/{start_date.month % 12 + 1}/{day}")
				self.expense.due_dates = [datetime.strptime(date, "%Y/%m/%d") for date in due_dates]
				next_due_date = self.expense.calculate_next_due_date(start_date)

				if next_due_date:
					self.assertEqual(next_due_date.day, day)

			start_date += delta


if __name__ == "__main__":
	unittest.main()
