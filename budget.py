from datetime import datetime
from datetime import timedelta
from calendar import monthrange
import toml
import argparse
from collections import defaultdict
import sys, os


def log(*args, **kwargs):
	with open('budget.log', 'a') as f:
		print(*args, **kwargs, file=f)
	print(*args, **kwargs)


def opt_today(date=None):
	if date:
		return date
	return datetime.today()


class Expense:
	def __init__(self, name, envelope, amount, current_date, due_day=None, due_dates=None):
		self.name = name
		self.envelope = envelope
		self.due_day = due_day
		self.amount = amount
		if due_dates:
			year = opt_today(current_date).year
			self.due_dates = [datetime.strptime(f"{year}/{date}", "%Y/%m/%d") for date in due_dates]
		else:
			self.due_dates = None

	def calculate_next_due_date(self, current_date):
		today = current_date
		if self.due_dates:
			future_due_dates = [date.replace(year=today.year) for date in self.due_dates if date.replace(year=today.year) > today]
			if not future_due_dates:  # if all dates this year are in the past, check for the next year
				future_due_dates = [date.replace(year=today.year + 1) for date in self.due_dates]
			next_due_date = min(future_due_dates, default=None)
		elif self.due_day:
			if today.day <= self.due_day:
				next_due_date = today.replace(day=self.due_day)
			else:
				next_month = today.month + 1 if today.month < 12 else 1
				next_year = today.year if today.month < 12 else today.year + 1

				# check if self.due_day is not greater than max day of next_month
				_, last_day = monthrange(next_year, next_month)
				day = min(self.due_day, last_day)

				next_due_date = today.replace(month=next_month, year=next_year, day=day)
		else:
			return None
		return next_due_date


class Config:
	def __init__(self, file_path, current_date=None):
		self.file_path = file_path
		self.current_date = opt_today(current_date)
		self.envelopes = []
		self.pay_iso_week = None
		self.pay_day = None
		self.min_exp_saved = None
		self.expenses = []

	def load(self):
		data = toml.load(self.file_path)
		self.envelopes = data['config']['envelopes']
		self.pay_iso_week = data['config']['pay_iso_week']
		self.min_exp_saved = data['config']['min_expense_saved_percent']
		self.pay_day = data['config']['payday']
		for expense_data in data['expenses']:
			# If 'due_day' present, and it exceeds the last day of current month
			if 'due_day' in expense_data and expense_data['due_day'] > \
					monthrange(self.current_date.year, self.current_date.month)[1]:
				_, last_day_of_month = monthrange(self.current_date.year, self.current_date.month)
				expense_data['due_day'] = last_day_of_month

			expense = Expense(**expense_data, current_date=self.current_date)
			self.expenses.append(expense)

	def print_config(self):
		log("[config]")
		log(f"Envelopes: {self.envelopes}")
		log(f"Pay ISO Week: {self.pay_iso_week}")
		log(f"Min Expense Saved: {self.min_exp_saved}")
		log(f"{'Expense':<20} {'Envelope':<10} {'Due Day/Dates':<30} {'Amount':<10}")
		log("-"*75)
		for expense in self.expenses:
			due = expense.due_day if expense.due_day else ','.join([date.strftime("%m/%d") for date in expense.due_dates])
			log(f"{expense.name:<20} {expense.envelope:<10} {due:<30} {expense.amount:<10.2f}")
		log("-"*75)
		log("\n")

	def get_payday_number(self):
		days = ["Mon", "Tues", "Wed", "Thurs", "Fri", "Sat", "Sun"]
		if self.pay_day in days:
			return days.index(self.pay_day)
		else:
			raise ValueError(f"'{self.pay_day}' is not a valid day of format Mon, Tues, Wed, Thurs, Fri, Sat, Sun")

	def get_next_paydays(self, n):
		current_date = self.current_date
		payday_n = self.get_payday_number()
		paydays = []

		for _ in range(n):
			while True:
				current_date += timedelta(days=1)
				if current_date.weekday() == payday_n and ((current_date.isocalendar()[1] % 2 == 0 and self.pay_iso_week == "even") or (current_date.isocalendar()[1] % 2 != 0 and self.pay_iso_week == "odd")):
					break

			paydays.append(current_date)

		return paydays


class Budget:
	def __init__(self, config_path, current_date):
		self.config = Config(config_path, current_date=current_date)

		self.config.load()
		# self.config.print_config()

	def calculate_savings(self, expense):
		next_due_date = expense.calculate_next_due_date(self.config.current_date)
		paydays_before_due_date = 0

		if next_due_date:
			# get number of paydays before expense is due, EXCLUDING the due date
			paydays_before_due_date = sum(date < next_due_date for date in self.config.get_next_paydays(12))

			min_amount = self.config.min_exp_saved
			# savings curve
			percent_map = [1, 0.66, 0.40, 0.20, 0.10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

			percent_saved = percent_map[paydays_before_due_date]
			percent_saved = max(min_amount, percent_saved)

			return next_due_date, paydays_before_due_date, percent_saved, percent_saved * expense.amount
		else:
			raise ValueError(f"'{next_due_date}' is not a valid date")

	def calculate_envelopes(self):
		envelopes = defaultdict(float)

		log(f"\nExpense Savings as of ({self.config.current_date:%Y-%m-%d}):")
		log(f"{'Expense':<20} {'Envelope':<10} {'Due':<10} {'Checks Left':<11} {'Percent':<10} {'Amount':<10}")
		log("-"*65)

		for expense in self.config.expenses:
			envelope = expense.envelope
			due, checks_remaining, percent, savings = self.calculate_savings(expense)
			envelopes[envelope] += savings
			log(f"{expense.name:<20} {envelope:<10} {f'{due:%m/%d}':<10} {checks_remaining:<11} {percent:<10.2f} {savings:<10.2f}")

		log("-"*65)

		prnt = f"\nEnvelopes:\n{'Envelope':<15} {'Total Savings':<15} {'Remaining':<15}\n"
		prnt += "-" * 40
		prnt += '\n'
		for envelope, total_savings in envelopes.items():
			amt = int(input(f'enter current amount in {envelope}:\t'))
			log(f"{envelope} = {amt:.2f}")
			prnt += f"{envelope:<15} {total_savings:<15.2f} {max(0, total_savings - amt):<15.2f}\n"
		log(prnt, end='')
		log("-" * 40)


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("--today", help="manually set the current date")
	args = parser.parse_args()

	if args.today:
		today = datetime.strptime(args.today, "%Y-%m-%d")
	else:
		today = None

	budget = Budget("config.toml", current_date=today)
	budget.calculate_envelopes()


if __name__ == "__main__":

	if os.path.exists('budget.log'):
		os.remove('budget.log')

	main()
